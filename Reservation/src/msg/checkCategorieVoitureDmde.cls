Class msg.checkCategorieVoitureDmde Extends (Ens.Request, %JSON.Adaptor)
{

Property categorie As %String;

Property dateDebut As %Date;

Property dateFin As %Date;

Storage Default
{
<Data name="checkCategorieVoitureDmdeDefaultData">
<Subscript>"checkCategorieVoitureDmde"</Subscript>
<Value name="1">
<Value>categorie</Value>
</Value>
<Value name="2">
<Value>dateDebut</Value>
</Value>
<Value name="3">
<Value>dateFin</Value>
</Value>
</Data>
<DefaultData>checkCategorieVoitureDmdeDefaultData</DefaultData>
<Type>%Storage.Persistent</Type>
}

}
