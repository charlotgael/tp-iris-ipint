Class msg.checkReservationDmde Extends (Ens.Request, %JSON.Adaptor)
{

Property categorie As %String;

Property plaque As %String;

Property dateDebut As %Date(FORMAT = 15);

Property dateFin As %Date(FORMAT = 15);

Storage Default
{
<Data name="checkReservationDmdeDefaultData">
<Subscript>"checkReservationDmde"</Subscript>
<Value name="1">
<Value>categorie</Value>
</Value>
<Value name="2">
<Value>plaque</Value>
</Value>
<Value name="3">
<Value>dateDebut</Value>
</Value>
<Value name="4">
<Value>dateFin</Value>
</Value>
</Data>
<DefaultData>checkReservationDmdeDefaultData</DefaultData>
<Type>%Storage.Persistent</Type>
}

}
