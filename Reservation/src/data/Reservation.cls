Class data.Reservation Extends (%Persistent, %JSON.Adaptor)
{

Property categorie As %String(VALUELIST = "'2 places', 'Cabriolet', 'Berline', 'Monospace', '4x4', '7 places'");

Property plaque As %String(MAXLEN = 9, MINLEN = 9);

Property dateDebut As %Date(FORMAT = 15);

Property dateFin As %Date(FORMAT = 15);

Storage Default
{
<Data name="ReservationDefaultData">
<Value name="1">
<Value>%%CLASSNAME</Value>
</Value>
<Value name="2">
<Value>categorie</Value>
</Value>
<Value name="3">
<Value>plaque</Value>
</Value>
<Value name="4">
<Value>dateDebut</Value>
</Value>
<Value name="5">
<Value>dateFin</Value>
</Value>
</Data>
<DataLocation>^data.ReservationD</DataLocation>
<DefaultData>ReservationDefaultData</DefaultData>
<IdLocation>^data.ReservationD</IdLocation>
<IndexLocation>^data.ReservationI</IndexLocation>
<StreamLocation>^data.ReservationS</StreamLocation>
<Type>%Storage.Persistent</Type>
}

}
