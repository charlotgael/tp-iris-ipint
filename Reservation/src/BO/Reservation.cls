Class BO.Reservation Extends Ens.BusinessOperation
{

Property Adapter As Ens.OutboundAdapter;

Parameter ADAPTER = "Ens.OutboundAdapter";

Parameter INVOCATION = "Queue";

Method checkCategorieVoiture(pRequest As msg.checkCategorieVoitureDmde, Output pResponse As msg.checkCategorieVoitureRpse) As %Status
{
    
    Set pResponse = ##class(msg.checkCategorieVoitureRpse).%New()
    
    Set sql = "select plaque from data.Voiture where categorie = '"_pRequest.categorie_"' and plaque not in ("
    Set sql = sql_" SELECT voiture->plaque FROM data.Reservation where voiture->categorie = '"_pRequest.categorie_"'"
    Set sql = sql_" and ((dateDebut >= '"_pRequest.dateDebut_"' and dateDebut <= '"_pRequest.dateFin_"' ) "
    Set sql = sql_" or (dateFin >= '"_pRequest.dateDebut_"' and dateFin <= '"_pRequest.dateFin_"' ) "
    Set sql = sql_" or (dateDebut <= '"_pRequest.dateDebut_"' and dateFin >= '"_pRequest.dateFin_"' ) "
    Set sql = sql_" or (dateDebut >= '"_pRequest.dateDebut_"' and dateFin <= '"_pRequest.dateFin_"' )) "
    Set sql = sql_ " ) "

    Set statement = ##class(%SQL.Statement).%New()
    Do statement.%Prepare(sql)
    Set request = statement.%Execute()

    If request.%Next() {
        Set plaque = request.%Get("plaque")
        Set pResponse.plaque = plaque
        Set pResponse.codeRetour = "OK"
    } Else {
        Set pResponse.codeRetour = "KO"
        Set pResponse.libErreur = "Aucune voiture avec cette Categorie trouvée"
    }
    Quit $$$OK
}

Method reserverVoiture(pRequest As msg.reserverVoitureDmde, Output pResponse As msg.reserverVoitureRpse) As %Status
{
    
    Set pResponse = ##class(msg.reserverVoitureRpse).%New()

    Set reservation = ##class(data.Reservation).%New()
    Set reservation.plaque = pRequest.plaque
    Set reservation.categorie = pRequest.categorie
    Set reservation.dateDebut = pRequest.dateDebut
    Set reservation.dateFin = pRequest.dateFin
    Set tsc = reservation.%Save()

    If (tsc) {
        Set pResponse.plaque = pRequest.plaque
        Set pResponse.code = "OK"
    } Else {
        Set pResponse.code = "KO"
        Set pResponse.libErreur = "Erreur à l'enregistrement de la reservation"
    }

    Quit $$$OK
}

Method checkReservation(pRequest As msg.checkReservationDmde, Output pResponse As msg.checkReservationRpse) As %Status
{
    
    Set pResponse = ##class(msg.checkReservationRpse).%New()

    Set sql = "SELECT top_1_* FROM data.Reservation WHERE plaque = '"_pRequest.plaque_"' and categorie = '"_pRequest.categorie_"'"
    Set sql = sql_" AND (dateDebut >= now() or dateFin >= now())"

    Set statement = ##class(%SQL.Statement).%New()
    Do statement.%Prepare(sql)
    Set request = statement.%Execute()

    If request.%Next() {
        Set pResponse.codeRetour = "KO"
        Set pResponse.libErreur = "La voiture est déjà réservée"
    } Else {
        Set plaque = request.%Get("plaque")
        Set pResponse.plaque = plaque
        Set pResponse.codeRetour = "OK"
    }
    Quit $$$OK
}

XData MessageMap
{
<MapItems>
    <MapItem MessageType="msg.checkCategorieVoitureDmde">
        <Method>checkCategorieVoiture</Method>
    </MapItem>
    <MapItem MessageType="msg.reserverVoitureDmde">
        <Method>reserverVoiture</Method>
    </MapItem>
    <MapItem MessageType="msg.checkReservationDmde">
        <Method>checkReservation</Method>
    </MapItem>
</MapItems>
}

}
