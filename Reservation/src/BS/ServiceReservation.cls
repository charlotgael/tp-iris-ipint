Class BS.ServiceReservation Extends Ens.BusinessService
{

/// Name of the WebService
Parameter SERVICENAME = "ServiceReservation";

Method OnProcessInput(pInput As %RegisteredObject, Output pOutput As %RegisteredObject, ByRef pHint As %String) As %Status
{
    Set tsc = $CASE(pHint,
        "reserverVoiture":..reserverVoiture(pInput,.pOutput),
        "checkReservation":..checkReservation(pInput,.pOutput),
	 	: $$$ERROR($$$NotImplemented)
    )
    Quit tsc
}

Method reserverVoiture(pInput As %RegisteredObject, Output pOutput As %RegisteredObject) As %Status
{
    Set tsc = ..SendRequestSync("reserverVoiture",pInput,.pOutput)
    Quit $$$OK
}

Method checkReservation(pInput As %RegisteredObject, Output pOutput As %RegisteredObject) As %Status
{
    Set tsc = ..SendRequestSync("Reservation",pInput,.pOutput)
    Quit $$$OK
}

}
