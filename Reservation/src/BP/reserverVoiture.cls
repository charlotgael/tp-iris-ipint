/// 
Class BP.reserverVoiture Extends Ens.BusinessProcessBPL
{

/// BPL Definition
XData BPL [ XMLNamespace = "http://www.intersystems.com/bpl" ]
{
<process language='objectscript' request='msg.reserverVoitureDmde' response='msg.reserverVoitureRpse' height='2000' width='2000' >
<context>
<property name='plaque' type='%String' initialexpression='0' instantiate='0' >
<parameters>
<parameter name='MAXLEN'  value='50' />
</parameters>
</property>
</context>
<sequence xend='200' yend='800' >
<call name='DisponibiliteVoiture' target='Reservation' async='0' xpos='200' ypos='250' >
<request type='msg.checkCategorieVoitureDmde' >
<assign property="callrequest.categorie" value="request.categorie" action="set" />
<assign property="callrequest.categorie" value="response.voiture.categorie" action="set" />
</request>
<response type='msg.checkCategorieVoitureRpse' >
<assign property="response.codeRetour" value="callresponse.codeRetour" action="set" />
<assign property="response.libErreur" value="callresponse.libErreur" action="set" />
<assign property="response.voiture.plaque" value="callresponse.plaque" action="set" />
</response>
</call>
<if name='VoitureDisponible' condition='response.codeRetour="OK"' xpos='200' ypos='350' xend='200' yend='700' >
<true>
<call name='RéserverLaVoiture' target='Reservation' async='0' xpos='335' ypos='500' >
<request type='msg.reserverVoitureDmde' >
<assign property="callrequest.categorie" value="request.categorie" action="set" />
</request>
<response type='msg.reserverVoitureRpse' >
<assign property="response.codeRetour" value="callresponse.codeRetour" action="set" />
<assign property="response.libErreur" value="callresponse.libErreur" action="set" />
<assign property="response.voiture.marque" value="callresponse.voiture.marque" action="set" />
<assign property="response.voiture.modele" value="callresponse.voiture.modele" action="set" />
<assign property="response.voiture.categorie" value="callresponse.voiture.categorie" action="set" />
<assign property="response.voiture.plaque" value="callresponse.voiture.plaque" action="set" />
<assign property="response.voiture.couleur" value="callresponse.voiture.couleur" action="set" />
<assign property="response.voiture.nbPlaces" value="callresponse.voiture.nbPlaces" action="set" />
<assign property="request.categorie" value="callresponse.voiture.categorie" action="set" />
<assign property="context.plaque" value="callresponse.voiture.plaque" action="set" />
</response>
</call>
<call name='CheckReservation' target='Reservation' async='0' xpos='335' ypos='600' >
<request type='msg.checkReservationDmde' >
<assign property="callrequest.plaque" value="context.plaque" action="set" />
<assign property="callrequest.categorie" value="request.categorie" action="set" />
</request>
<response type='msg.checkReservationRpse' >
<assign property="response.codeRetour" value="callresponse.codeRetour" action="set" />
<assign property="response.libErreur" value="callresponse.libErreur" action="set" />
<assign property="context.plaque" value="callresponse.plaque" action="set" />
<assign property="response.voiture.plaque" value="callresponse.plaque" action="set" />
</response>
</call>
</true>
</if>
</sequence>
</process>
}

Storage Default
{
<Type>%Storage.Persistent</Type>
}

}
