Class msg.verificationVoitureDmde Extends (Ens.Request, %JSON.Adaptor)
{

Property plaque As %String;

Storage Default
{
<Data name="verificationVoitureDmdeDefaultData">
<Subscript>"verificationVoitureDmde"</Subscript>
<Value name="1">
<Value>plaque</Value>
</Value>
</Data>
<DefaultData>verificationVoitureDmdeDefaultData</DefaultData>
<Type>%Storage.Persistent</Type>
}

}
