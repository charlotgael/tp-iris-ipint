Class msg.verificationVoitureRpse Extends (msg.basic.reponse, %JSON.Adaptor)
{

Property voiture As list Of webObjet.wsVoiture;

Storage Default
{
<Data name="verificationVoitureRpseDefaultData">
<Subscript>"verificationVoitureRpse"</Subscript>
<Value name="1">
<Value>voiture</Value>
</Value>
</Data>
<DefaultData>verificationVoitureRpseDefaultData</DefaultData>
<Type>%Storage.Persistent</Type>
}

}
