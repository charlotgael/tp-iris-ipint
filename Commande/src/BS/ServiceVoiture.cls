Class BS.ServiceVoiture Extends Ens.BusinessService
{

/// Name of the WebService
Parameter SERVICENAME = "ServiceVoiture";

Method OnProcessInput(pInput As %RegisteredObject, Output pOutput As %RegisteredObject, ByRef pHint As %String) As %Status
{
    Set tsc = $CASE(pHint,
        "CommandeVoiture":..CommandeVoiture(pInput,.pOutput),
        "SendVoiture":..SendVoiture(pInput, pOutput),
         : $$$ERROR($$$NotImplemented)
    )
    Quit tsc
}

Method CommandeVoiture(pInput As %RegisteredObject, Output pOutput As %RegisteredObject) As %Status
{
    Set tsc = ..SendRequestSync("CreerVoiture",pInput,.pOutput)
    Quit $$$OK
}

Method SendVoiture(pInput As %RegisteredObject, Output pOutput As %RegisteredObject) As %Status
{
    Set tsc = ..SendRequestSync("ProcessSendVoiture",pInput,.pOutput)
    Quit $$$OK
}

}
