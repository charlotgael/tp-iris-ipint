Class BS.API Extends (REST.Base, Ens.BusinessService)
{

Parameter Version = "1.0.6";

XData UrlMap [ XMLNamespace = "http://www.intersystems.com/urlmap" ]
{
<Routes>
<!-- Server Info -->
<Route Url="/" Method="GET" Call="GetInfo" Cors="true"/>
<!-- Swagger specs -->
<Route Url="/_spec" Method="GET" Call="SwaggerSpec" />
<!-- Créer une voiture-->
<Route Url="/catalogue/" Method="get" Call="GetCatalogue"/>
<!-- Commander une voiture-->
<Route Url="/voiture/" Method="POST" Call="CommanderVoiture"/>
<!-- Envoyer une voiture hors du parc -->
<Route Url="/sendvoiture/" Method="POST" Call="SendVoiture"/>
</Routes>
}

/// PersonsREST general information
ClassMethod GetInfo() As %Status
{
  Set version = ..#Version
  Set info = {
    "version": (version)
  }
  Return ..%ProcessResult($$$OK, info)
}

ClassMethod SwaggerSpec() As %Status
{
  Set tSC = ##class(%REST.API).GetWebRESTApplication($NAMESPACE, %request.Application, .swagger)
  Do swagger.info.%Remove("x-ISC_Namespace")
  Set swagger.basePath = "/crud"
  Set swagger.info.title = "InterSystems IRIS REST CRUD demo"
  Set swagger.info.version = "0.1"
  Set swagger.host = "localhost:52773"
  Return ..%ProcessResult($$$OK, swagger)
}

ClassMethod GetCatalogue() As %Status
{
	  #dim tSC As %Status = $$$OK
    Set msg = ##class(msg.getCatalogueDmde).%New()
    Set tSC = ##class(Ens.Director).CreateBusinessService("ServiceCatalogue",.tService) If $$$ISERR(tSC) Quit 
    Set tSC = tService.ProcessInput(msg,.response,"GetCatalogue")

    Set %response.Status = 200
    Set %response.ContentType = ..#CONTENTTYPEJSON
    Do response.%JSONExport()

    Quit tSC
}

ClassMethod CommanderVoiture() As %Status
{
      #dim tSC As %Status = $$$OK
    Set msg = ##class(msg.commandeDmde).%New()


    //On récupére les données de la requête
    Set data=%request.Content
    Do msg.%JSONImport(data)
    //$$$TOE(tSC,msg.%JSONImport(data))


    Set tSC = ##class(Ens.Director).CreateBusinessService("ServiceVoiture",.tService) If $$$ISERR(tSC) Quit tSC
    Set tSC = tService.ProcessInput(msg,.response,"CommandeVoiture")

    Set %response.Status = 200
    Set %response.ContentType = ..#CONTENTTYPEJSON
    Do response.%JSONExport()

    Quit tSC
}

ClassMethod SendVoiture() As %Status
{
    #dim tSC As %Status = $$$OK
    Set msg = ##class(msg.processSendVoitureDmde).%New()


    //On récupére les données de la requête
    Set data=%request.Content
    Do msg.%JSONImport(data)
    //$$$TOE(tSC,msg.%JSONImport(data))


    Set tSC = ##class(Ens.Director).CreateBusinessService("ServiceVoiture",.tService) If $$$ISERR(tSC) Quit tSC
    Set tSC = tService.ProcessInput(msg,.response,"SendVoiture")

    Set %response.Status = 200
    Set %response.ContentType = ..#CONTENTTYPEJSON
    Do response.%JSONExport()

    Quit tSC
}

}
