Class BO.Reservation Extends EnsLib.REST.Operation
{

Property Adapter As EnsLib.HTTP.OutboundAdapter;

Parameter ADAPTER = "EnsLib.HTTP.OutboundAdapter";

Parameter INVOCATION = "Queue";

Method creerVoiture(pRequest As msg.creerVoitureDmde, Output pResponse As msg.creerVoitureRpse) As %Status
{
    Set pResponse = ##class(msg.creerVoitureRpse).%New()
    // Transform Object ws TO JSON de notre request
    
    Set httpRequest = ##class(%Net.HttpRequest).%New()

    Set request = ##class(msg.reservationVoitureDmde).%New()

    Set request.nom = pRequest.voiture.marque_" "_pRequest.voiture.modele
    Set request.categorie = pRequest.voiture.categorie
    Set request.plaque = pRequest.voiture.plaque

    Do ..ObjectToJSONStream(request,.EntityBody)

    Set httpRequest.EntityBody = EntityBody

    // Transform Reponse JSON TO ws Object de notre reponse
    Set tsc = ..Adapter.SendFormDataArray(.httpReponse, "POST", httpRequest)
    Do ..JSONStreamToObject(httpReponse.Data,.pResponse,"msg.creerVoitureRpse",1)
    If (tsc) {
        Set pResponse.codeRetour = "OK"
    } Else {
        Set pResponse.codeRetour = "KO"
    }
    Quit $$$OK
}

XData MessageMap
{
<MapItems>
    <MapItem MessageType="msg.creerVoitureDmde">
        <Method>creerVoiture</Method>
    </MapItem>
</MapItems>
}

}
