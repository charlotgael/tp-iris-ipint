Class BO.Verification Extends EnsLib.REST.Operation
{

Property Adapter As EnsLib.HTTP.OutboundAdapter;

Parameter ADAPTER = "EnsLib.HTTP.OutboundAdapter";

Parameter INVOCATION = "Queue";

Method verificationReservation(pRequest As msg.verificationVoitureDmde, Output pResponse As msg.verificationVoitureRpse) As %Status
{
    Set pResponse = ##class(msg.verificationVoitureRpse).%New()
    Set httpRequest = ##class(%Net.HttpRequest).%New()
    Set request = ##class(msg.reservationVoitureDmde).%New()
    Set request.plaque = pRequest.plaque
    Do ..ObjectToJSONStream(request, .EntityBody)
    Set httpRequest.EntityBody = EntityBody

    Set tsc = ..Adapter.SendFormDataArray(.httpResponse, "POST", httpRequest)

    Do ..ObjectToJSONStream(httpResponse.Data, .pResponse, "msg.verificationVoitureRpse", 1)
    
    Quit $$$OK
}

XData MessageMap
{
<MapItems>
    <MapItem MessageType="Ens.Request">
        <Method>verificationReservation</Method>
    </MapItem>
</MapItems>
}

}
