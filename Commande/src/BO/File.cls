Class BO.File Extends Ens.BusinessOperation
{

Property Adapter As EnsLib.File.OutboundAdapter;

Parameter ADAPTER = "EnsLib.File.OutboundAdapter";

Parameter INVOCATION = "Queue";

Method creerVoiture(pRequest As msg.pushVoitureFileDmde, Output pResponse As msg.pushVoitureFileRpse) As %Status
{
    Set pResponse = ##class(msg.pushVoitureFileRpse).%New()
    //create file
    Set filename="fichierVoiture"_$TRANSLATE($TRANSLATE($ZDATETIME($ZDATETIMEH($ZTIMESTAMP,-3),3,9),":/-","")," ","_")_".csv"
    Set file=##class(%Library.FileBinaryStream).%New()
    Do file.Write(pRequest.voiture.marque_";"_pRequest.voiture.modele_";"_pRequest.voiture.categorie_";"_pRequest.voiture.plaque_";"_pRequest.voiture.couleur_";"_pRequest.voiture.nbPlaces)

    Set tsc = ..Adapter.PutStream(filename,file)
    If (tsc) {
        Set pResponse.codeRetour = "OK"
    } Else {
        Set pResponse.codeRetour = "KO"
    }
    Quit $$$OK
}

XData MessageMap
{
<MapItems>
    <MapItem MessageType="msg.pushVoitureFileDmde">
        <Method>creerVoiture</Method>
    </MapItem>
</MapItems>
}

}
