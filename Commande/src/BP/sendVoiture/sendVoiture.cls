/// 
Class BP.sendVoiture.sendVoiture Extends Ens.BusinessProcessBPL
{

/// BPL Definition
XData BPL [ XMLNamespace = "http://www.intersystems.com/bpl" ]
{
<process language='objectscript' request='msg.processSendVoitureDmde' response='Ens.Response' height='2000' width='2000' >
<sequence xend='200' yend='700' >
<call name='VerificationVoiture' target='Reservation' async='0' xpos='200' ypos='250' >
<request type='msg.verificationVoitureDmde' >
<assign property="callrequest.plaque" value="request.plaque" action="set" />
</request>
<response type='Ens.Response' />
</call>
<if name='VoitureExiste' condition='response.codeRetour="OK"' xpos='200' ypos='350' xend='200' yend='600' >
<true>
<call name='SupprimerVoiture' target='Voiture' async='0' xpos='335' ypos='500' >
<request type='msg.supprimerVoitureCatalogueDmde' />
<response type='msg.supprimerVoitureRpse' />
</call>
</true>
</if>
</sequence>
</process>
}

Storage Default
{
<Type>%Storage.Persistent</Type>
}

}
